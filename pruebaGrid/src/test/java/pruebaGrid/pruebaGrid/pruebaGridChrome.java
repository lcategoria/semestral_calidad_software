package pruebaGrid.pruebaGrid;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class pruebaGridChrome {

public static void main(String[] args) throws MalformedURLException {
		
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setBrowserName("chrome");
		cap.setPlatform(Platform.WIN10);
		
		ChromeOptions options = new ChromeOptions();
		options.merge(cap);
		
		String hubURL = "http://192.168.0.9:4444/";
		WebDriver driver = new RemoteWebDriver(new URL(hubURL), options);
		
		driver.get("https://www.google.com/");
		System.out.println(driver.getTitle());
		
		//driver.quit();
	}

}
