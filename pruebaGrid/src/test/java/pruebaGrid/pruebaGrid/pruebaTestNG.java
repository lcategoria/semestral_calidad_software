package pruebaGrid.pruebaGrid;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class pruebaTestNG {
   public WebDriver driver;
   public String URL, Node;
   protected ThreadLocal<RemoteWebDriver> threadDriver = null;
   
   @Parameters("browser")
   @BeforeTest
   public void launchapp(String browser) throws MalformedURLException {
      String URL = "https://www.way2automation.com/protractor-angularjs-practice-website.html";
      
      if (browser.equalsIgnoreCase("chrome")) {
         System.out.println(" Executing on Chrome");
         DesiredCapabilities cap = DesiredCapabilities.chrome();
         cap.setBrowserName("chrome");
         cap.setPlatform(Platform.WIN10);
         String Hub = "http://172.30.192.1:4444";
         driver = new RemoteWebDriver(new URL(Hub), cap);
         driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
         
         // Launch website
         driver.navigate().to(URL);
         driver.manage().window().maximize();
         
      } else if (browser.equalsIgnoreCase("MicrosoftEdge")) {
         System.out.println(" Executing on Microsoft Edge");
         DesiredCapabilities cap = DesiredCapabilities.edge();
         cap.setBrowserName("MicrosoftEdge");
         cap.setPlatform(Platform.WIN10);
         String Hub = "http://172.30.192.1:4444";
         driver = new RemoteWebDriver(new URL(Hub), cap);
         driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
         
         // Launch website
         driver.navigate().to(URL);
         driver.manage().window().maximize();
      } else {
         throw new IllegalArgumentException("The Browser Type is Undefined");
      }
   }
   
   @Test
   public void registration() throws InterruptedException {
	   String originalWindow=driver.getWindowHandle(); 
	   driver.findElement(By.xpath("//*[@id=\"wrapper\"]/div[2]/div[2]/div/ul/li[2]/a")).click();
	   
	   for (String windowHandle : driver.getWindowHandles()) {
		    if(!originalWindow.contentEquals(windowHandle)) {
		        driver.switchTo().window(windowHandle);
		        break;
		    }
		}     	
      
	   //asdasd
      driver.findElement(By.xpath("/html/body/div[3]/div/div/div/form/div[1]/input")).sendKeys("angular");
      driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys("password");
      driver.findElement(By.name("formly_1_input_username_0")).sendKeys("asdf");
      driver.findElement(By.xpath("/html/body/div[3]/div/div/div/form/div[3]/button")).click();
      
      
      
      // Get the Result Text based on its xpath
      TimeUnit.SECONDS.sleep(4);
      WebElement result =
         driver.findElement(By.xpath("/html/body/div[3]/div/div/div/p[2]/a"));
      	assertTrue(result.isDisplayed());
      // Print a Log In message to the screen

   driver.close();
   driver.switchTo().window(originalWindow);
   }
   
   
   
   @Test
   public void registration2() throws InterruptedException {
	   String originalWindow=driver.getWindowHandle(); 
	   driver.findElement(By.xpath("/html/body/section/div[2]/div[2]/div/ul/li[3]/a")).click();
	   
	   for (String windowHandle : driver.getWindowHandles()) {
		    if(!originalWindow.contentEquals(windowHandle)) {
		        driver.switchTo().window(windowHandle);
		        break;
		    }
		}     	
      
	   //asdasd
      driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div/form/div/div[1]/input")).sendKeys("Pepin");
      driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div/form/div/div[2]/input")).sendKeys("pepin@hotmail.com");
      driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div/form/div/div[3]/div/a")).click();
      driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div/form/div/div[1]/div[1]/label/input")).click();
      driver.findElement(By.xpath("/html/body/div[3]/div/div/div/div/form/div/div[2]/div/a")).click();

      // Get the Result Text based on its xpath
      TimeUnit.SECONDS.sleep(4);
      String result =
         driver.findElement(By.xpath("/html/body/div[3]/div/div/div/pre")).getText();
      	assertTrue(result.contains("{\"name\":\"Pepin\",\"email\":\"pepin@hotmail.com\",\"type\":\"xbox\"}"));
     
      driver.close();
      driver.switchTo().window(originalWindow);
   }
   
   @Test
   public void registration3() throws InterruptedException {
	   String originalWindow=driver.getWindowHandle(); 
	   driver.findElement(By.xpath("//*[@id=\"wrapper\"]/div[2]/div[2]/div/ul/li[4]/a")).click();
	   
	   for (String windowHandle : driver.getWindowHandles()) {
		    if(!originalWindow.contentEquals(windowHandle)) {
		        driver.switchTo().window(windowHandle);
		        break;
		    }
		}     	
      
	   //asdasd
      driver.findElement(By.xpath("/html/body/div[3]/div/form/input[1]")).sendKeys("20");
      driver.findElement(By.xpath("/html/body/div[3]/div/form/input[2]")).sendKeys("20");
      driver.findElement(By.xpath("//*[@id=\"gobutton\"]")).click();
      
      
      
      // Get the Result Text based on its xpath
      TimeUnit.SECONDS.sleep(4);
      String result =
         driver.findElement(By.xpath("/html/body/div[3]/div/form/h2")).getText();
      	assertEquals("40",result);
      // Print a Log In message to the screen

   driver.close();
   driver.switchTo().window(originalWindow);
   }
   
   @AfterTest
   public void closeBrowser() {
      driver.quit();
   }
}


